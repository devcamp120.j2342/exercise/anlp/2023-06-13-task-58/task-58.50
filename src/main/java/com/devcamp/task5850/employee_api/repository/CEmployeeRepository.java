package com.devcamp.task5850.employee_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5850.employee_api.model.CEmployee;

public interface CEmployeeRepository extends JpaRepository<CEmployee, Long> {
    
}
