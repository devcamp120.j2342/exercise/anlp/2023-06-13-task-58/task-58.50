package com.devcamp.task5850.employee_api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.devcamp.task5850.employee_api.model.CEmployee;
import com.devcamp.task5850.employee_api.repository.CEmployeeRepository;
@CrossOrigin
@RestController
public class CEmployeController {
    @Autowired
    CEmployeeRepository pEmployeeRepository;

    @GetMapping("/employees")
    public ResponseEntity<List<CEmployee>> getAllEmployee() {
         try {
            List<CEmployee> pEmployee = new ArrayList<>();
            pEmployeeRepository.findAll().forEach(pEmployee::add);
            return new ResponseEntity<>(pEmployee, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
